package com.soheil.library;

public class VariableSample {

    public static void main(String[] args) {
        isALive();
        name();
        number();
        pNumber();
        print("My Name IS " + name());
        print("Number IS " + number());
        print("Is A Live " + isALive());
        print("Pnumber IS " + pNumber());

    }

    private static boolean isALive() {
        return true;
    }

    private static String name() {
        return "Soheil";
    }

    private static int number() {
        return 1234567890;
    }

    private static double pNumber() {
        return 3.14444444;
    }

    private static void print(String msg) {
        System.out.println(msg);
    }

    private static void print(int msg) {
        System.out.println(msg);
    }

    private static void print(boolean msg) {
        System.out.println(msg);
    }

    private static void print(double msg) {
        System.out.println(msg);
    }

}
