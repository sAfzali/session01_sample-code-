package com.soheil.library;

public class VariableStatments {

    public static void main(String[] args) {
        int age = 25;
        print(getTypeByAge(age));
    }

    private static String getTypeByAge(int age) {
        if (age <= 10) {
            return "Child";
        } else if (age > 10 && age <= 19) {
            return "Teenage";
        } else if (age > 19 && age <= 40) {
            return "Young";
        } else if (age > 40) {
            return "Old";
        }
        return "Error";
    }

    private static void print(String msg) {
        System.out.println(msg);
    }
}
